import React from "react";


function SalespeopleList(props) {
    if (props.salespeople === undefined) {
        return null
    }


    return (
        <main>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {props.salespeople.map(salesperson => {
                        return (
                            <tr key={ salesperson.id }>
                                 <td>{ salesperson.employee_id }</td>
                                 <td>{ salesperson.first_name}</td>
                                 <td>{ salesperson.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </main>
    )

}

export default SalespeopleList;
