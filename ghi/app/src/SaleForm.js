import React, {useState, useEffect } from 'react'

// Special Feature 1: UNSOLD ONLY.
// Should already be implemented on the back-end (in views.py api_list_sales line 153)
//  but pls test it, MK
// Update, you changed your dadgum view trying to troubleshoot that other thing and got rid of that functionality. Dangit.
// Upon testing, added ID to the select menu for Customer.
//      Customers are appearing once for each sale rather than once for each customer id.
//      Alright. I added additional props for salespeople and customers to App.js in order to map it through the correct entity.
// Okay. I've changed pretty much everything since then. I'm getting a 400 error on submit, which I believe is due to something with automobile....
// Okay, I had to change the keys on each select to match the attributes in my views (e.g. Instead of salesperson.id use salesperson.employee_id)



function SaleForm() {
    const [autos, setAutos] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile , setAutomobile] = useState('')
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const saleUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };

        const response = await fetch(saleUrl, fetchConfig);

        const autoData = {};
        autoData.sold = true;

        const automobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`
        const autoConfig = {
            method: "put",
            body: JSON.stringify(autoData),
            headers: {
                "Content-Type": 'application/json',
            },
        };

        const autoResponse = await fetch(automobileUrl, autoConfig);

        if (response.ok && autoResponse.ok) {
            await response.json();

            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    }

    const fetchData = async () => {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const autoResponse = await fetch(autoUrl);
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const salespersonResponse = await fetch(salespersonUrl);
        const customerUrl = "http://localhost:8090/api/customers/";
        const customerResponse = await fetch(customerUrl);

        if (autoResponse.ok && salespersonResponse.ok && customerResponse.ok) {
            const autoData = await autoResponse.json();
            setAutos(autoData.autos);
            const salespersonData = await salespersonResponse.json();
            setSalespeople(salespersonData.salespeople);
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers);

            }
        }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Record a Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
                <div className="form-floating mb-3">
                    <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select">
                        <option value="">Choose an automobile VIN</option>
                        {autos.map(auto => {
                            if (auto.sold === false) {
                        return (
                            <option key={auto.vin} value={auto.vin}>
                                vin: {auto.vin} sold: {auto.sold ? "yes" : "no" }
                            </option>
                        );}
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleSalespersonChange} value={salesperson} required id="salesperson" name="salesperson" className="form-select">
                        <option value="">Choose a salesperson</option>
                        {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                { salesperson.first_name} { salesperson.last_name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                        <option value="">Choose a customer</option>
                        {customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>
                                { customer.first_name} { customer.last_name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3" >
                    <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" id="price" name="price"  className="form-control" />
                    <label htmlFor="price" >Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>

    )

}

export default SaleForm;
