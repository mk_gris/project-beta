import React from 'react'

export default function MfrList(props) {
    if (props.vehicles === undefined) {
        return null
    }

    return (
        <>
                <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Manufacturer Name</th>
                        <th>Model Year</th>
                        <th>Manufacturer</th>
                        <th>Sold?</th>
                    </tr>
                </thead>
                <tbody>
                    {props.vehicles.map(vehicle => {
                        return (
                            <tr key={vehicle.id}>
                                <td>{vehicle.vin}</td>
                                <td>{vehicle.color}</td>
                                <td>{vehicle.model.name}</td>
                                <td>{vehicle.year}</td>
                                <td>{vehicle.model.manufacturer.name}</td>
                                <td>{vehicle.sold ? "yes" : "no" }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
