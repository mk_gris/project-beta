import React, { useState } from 'react'


function ApptList(props) {

    const [visNotification, setVisNotification] = useState(false)

    if (props.appts === undefined) {
        return null
    }



    async function handleCancelBtn(apptId) {
        try {
            const cancelUrl = `http://localhost:8080/api/appointments/${apptId}/cancel/`
            const response = await fetch(cancelUrl, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            if (response.ok) {
                setVisNotification(true)
                }
            } catch (e) {
            console.error('error canceling appt', e)
        }
    }

    async function handleFinishBtn(apptId) {
        try {
            const finishUrl = `http://localhost:8080/api/appointments/${apptId}/finish/`
            const response = await fetch(finishUrl, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            if (response.ok) {
                setVisNotification(true)
            }
        } catch (e) {
            console.error('')
        }
    }

    return (
<>
<div>
    <h3 className='text-center'>Current Appointments</h3>
<table className='table table-striped mx-1'>
<thead>
    <tr>
        <th>VIN</th>
        <th>VIP?</th>
        <th>Customer</th>
        <th>Date</th>
        <th>Time</th>
        <th>Technician</th>
        <th>Reason</th>
    </tr>
</thead>
<tbody>
    {props.appts.map(appt => {
        return (
            <tr key={appt.id}>
                <td>{appt.vin}</td>
                <td>{appt.is_vip ? 'Yes' : 'No'}</td>
                <td>{appt.customer}</td>
                <td>{appt.date}</td>
                <td>{appt.time}</td>
                <td>{appt.technician.first_name}</td>
                <td>{appt.reason}</td>
                <td><button className='btn btn-danger' onClick={() => handleCancelBtn(appt.id)}>Cancel</button></td>
                <td><button className='btn btn-success' onClick={() => handleFinishBtn(appt.id)}>Finish</button></td>
            </tr>
        )
    })}
</tbody>
</table>

</div>
</>
    )
}


export default ApptList
