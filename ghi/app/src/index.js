import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './modelList.css'
import './nav.css'



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadData() {
  const mfrResponse = await fetch('http://localhost:8100/api/manufacturers/')
  const techResponse = await fetch('http://localhost:8080/api/technicians/')
  const modelResponse = await fetch('http://localhost:8100/api/models/')
  const vehResponse = await fetch('http://localhost:8100/api/automobiles/')
  const apptResponse = await fetch('http://localhost:8080/api/appointments/')
  const saleResponse = await fetch('http://localhost:8090/api/sales/')
  const salespersonResponse = await fetch('http://localhost:8090/api/salespeople/')
  const customerResponse = await fetch('http://localhost:8090/api/customers/')

  if (
    mfrResponse.ok &&
    techResponse.ok &&
    modelResponse.ok &&
    vehResponse.ok &&
    apptResponse.ok &&
    saleResponse.ok &&
    salespersonResponse.ok &&
    customerResponse.ok
    ) {


    const mfrData = await mfrResponse.json()
    const techData = await techResponse.json()
    const modelData = await modelResponse.json()
    const vehData = await vehResponse.json()
    const apptData = await apptResponse.json()
    const saleData = await saleResponse.json()
    const salespersonData = await salespersonResponse.json()
    const customerData = await customerResponse.json()

    root.render(
        <App
        mfrs={mfrData.manufacturers}
        techs={techData.technicians}
        models={modelData.models}
        vehicles={vehData.autos}
        appts={apptData.appointments}
        sales={saleData.sales}
        salespeople={salespersonData.salespeople}
        customers={customerData.customers}
        />
    )
  } else {
    console.warn('response not ok')
  }
}
loadData()
