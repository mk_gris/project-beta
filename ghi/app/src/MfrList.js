import React from 'react'

export default function MfrList(props) {
    if (props.mfrs === undefined) {
        return null
    }

    return (
        <>
        <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Manufacturer Name</th>
                    </tr>
                </thead>
                <tbody>
                    {props.mfrs.map(mfrs => {
                        return (
                            <tr key={mfrs.id}>
                                <td>{ mfrs.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}