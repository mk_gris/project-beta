import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddAppt from './AddAppt';
import AddTech from './AddTech'
import MfrList from './MfrList';
import AddMfr from './AddMfr';
import TechsList from './TechsList';
import ModelsList from './ModelsList';
import AddModel from './AddModel';
import VehList from './VehList'
import AddVeh from './AddVeh'
import ApptList from './ApptList';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import ListSales from './SalesList';
import SaleForm from './SaleForm';
import SalespersonHistory from './SalespersonHistory';
import SvcHistory from './SvcHistory';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/techsList' element={<TechsList techs={props.techs} />} />
          <Route path='/addTech' element={<AddTech />} />
          <Route path='/apptList' element={<ApptList appts={props.appts} />} />
          <Route path='/addAppt' element={<AddAppt />} />
          <Route path='/svcHistory' element={<SvcHistory appts={props.appts} />} />
          <Route path='/mfrList' element={<MfrList  mfrs={props.mfrs} />} />
          <Route path='/addMfr' element={<AddMfr />} />
          <Route path='/modelsList' element={<ModelsList  models={props.models} />} />
          <Route path='/addModel' element={<AddModel />} />
          <Route path='/vehList' element={<VehList  vehicles={props.vehicles} />} />
          <Route path='/addVeh' element={<AddVeh />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/salespeople" element={<SalespeopleList salespeople={props.salespeople} />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/customers" element={<CustomerList customers={props.customers} />} />
          <Route path="/sales" element={<ListSales sales={props.sales} />} />
          <Route path="/sales/new" element={<SaleForm  />} />
          <Route path="/salespeople/history" element={<SalespersonHistory sales={props.sales} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
