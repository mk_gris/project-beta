import React, { useEffect, useState } from 'react'

function AddTech() {

    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('')
    const [tech, setTech] = useState('')
    const [techs, setTechs] = useState([])



    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value
        setTime(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }
    const handleTechChange = (event) => {
        const value = event.target.value
        setTech(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechs(data.technicians)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const submitBtnHandler = async (event) => {
        event.preventDefault()

        const data = {}
        data.vin = vin
        data.customer = customer
        data.date = date
        data.time = time
        data.reason = reason
        data.technician = tech


        const apptUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(apptUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()

            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setReason('')
            setTech('')

        }
    }

    return (
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
    <h1>Add An Appointment</h1>
    <form onSubmit={submitBtnHandler} id="create-hat-form">
    <div className="form-floating mb-3">
        <input onChange={handleVinChange} value={vin} placeholder="VIN..." required type="text" id="VIN" name="VIN"  className="form-control"/>
        <label htmlFor="name">VIN</label>
    </div>
    <div className="form-floating mb-3">
        <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" id="customer" name="customer"  className="form-control"/>
        <label htmlFor="fabric">Customer</label>
    </div>
    <div className="form-floating mb-3">
        <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" id="date" name="date"  className="form-control"/>
        <label htmlFor="style_name">Date</label>
    </div>
    <div className="form-floating mb-3">
        <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" id="time" name="time"  className="form-control"/>
        <label htmlFor="style_name">Time</label>
    </div>
    <div className="form-floating mb-3">
        <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" id="reason" name="reason"  className="form-control"/>
        <label htmlFor="style_name">Reason</label>
    </div>
    <div className="form-floating mb-3">
            <select onChange={handleTechChange} value={tech} required id="technician" name="technician" className="form-select">
                <option value="">Choose A Technician</option>
                {techs.map(tech => {
                    return (
                        <option key={tech.employee_id} value={tech.employee_id}>
                            {tech.first_name}
                        </option>
                    );
                })}
            </select>
            </div>
    <button className="btn btn-primary">Create</button>
    </form>
</div>
</div>
</div>
    );
}

export default AddTech;